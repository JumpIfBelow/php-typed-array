<?php

namespace TypedArray;

use ArrayAccess;
use ArrayIterator;
use Countable;
use IteratorAggregate;
use TypedArray\Exception\WrongTypeException;
use TypedArray\Exception\WrongValueException;

/**
 * Class TypedArray
 *
 * @package TypedArray
 */
class TypedArray implements ArrayAccess, Countable, IteratorAggregate
{
    public const TYPE_ARRAY = 0;
    public const TYPE_BOOL = 1;
    public const TYPE_CALLABLE = 2;
    public const TYPE_FLOAT = 3;
    public const TYPE_INT = 4;
    public const TYPE_ITERABLE = 5;
    public const TYPE_NULL = 6;
    public const TYPE_NUMERIC = 7;
    public const TYPE_OBJECT = 8;
    public const TYPE_RESOURCE = 9;
    public const TYPE_SCALAR = 10;
    public const TYPE_STRING = 11;

    private const AVAILABLE_TYPES = [
        self::TYPE_ARRAY,
        self::TYPE_BOOL,
        self::TYPE_CALLABLE,
        self::TYPE_FLOAT,
        self::TYPE_INT,
        self::TYPE_ITERABLE,
        self::TYPE_NULL,
        self::TYPE_NUMERIC,
        self::TYPE_OBJECT,
        self::TYPE_RESOURCE,
        self::TYPE_SCALAR,
        self::TYPE_STRING,
    ];

    /**
     * @var int|string|null $type
     */
    protected $type;

    /**
     * @var callable $checker
     */
    protected $checker;

    /**
     * @var array $storage
     */
    protected $storage;

    /**
     * TypedArray constructor.
     *
     * @param $checkMethod int|string|callable The type or the checker for the array. {@see TypedArray::setChecker()}
     * for more information about the checker callback.
     * @param array $storage The content of the array. Be aware that if the passed values are wrong given the checker,
     * an exception will be thrown.
     * @throws WrongTypeException
     * @throws WrongValueException
     */
    public function __construct($checkMethod, array $storage = [])
    {
        $this->storage = $storage;
        $this->setCheckMethod($checkMethod);
    }

    /**
     * Return the type if set by constructor or setter.
     * If the type is more complex and set by the checker, null will be returned.
     *
     * @return int|string|null
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return callable
     */
    public function getChecker()
    {
        return $this->checker;
    }

    /**
     * @param int|string|callable $checkMethod One of {@see TypedArray} constants, fully-qualified class name or a callable.
     * If it is a callable, see {@see TypedArray::setChecker()} for the callable signature.
     *
     * @return $this
     * @throws WrongTypeException If the given type is wrong.
     * @throws WrongValueException If the contained value are not validated by the new check method.
     */
    public function setCheckMethod($checkMethod)
    {
        if (is_callable($checkMethod)) {
            $this->setChecker($checkMethod);
        } else {
            $this->setType($checkMethod);
        }

        return $this;
    }

    /**
     * Cleans the storage but keep type constraint. Better to quickly empty an array.
     *
     * @return void
     */
    public function clear()
    {
        $this->storage = [];
    }

    /**
     * Converts the TypedArray to a native array.
     * This operation loses the type validation.
     *
     * @return array The array containing copied values.
     */
    public function toArray(): array
    {
        return $this->storage;
    }

    /**
     * @inheritdoc
     */
    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->storage);
    }

    /**
     * @inheritdoc
     */
    public function offsetGet($offset)
    {
        return $this->storage[$offset];
    }

    /**
     * @inheritdoc
     * @throws WrongValueException If the given value cannot be validated by the checker.
     */
    public function offsetSet($offset, $value)
    {
        if (!$this->validateValue($value, $offset)) {
            throw new WrongValueException();
        }

        if ($offset === null) {
            $this->storage[] = $value;
        } else {
            $this->storage[$offset] = $value;
        }
    }

    /**
     * @inheritdoc
     */
    public function offsetUnset($offset)
    {
        unset($this->storage[$offset]);
    }

    /**
     * @inheritdoc
     */
    public function count()
    {
        return count($this->storage);
    }

    /**
     * @inheritdoc
     */
    public function getIterator()
    {
        return new ArrayIterator($this->storage);
    }

    /**
     * @param int|string $type One of the {@see TypedArray} constants or fully-qualified class name.
     *
     * @return $this
     * @throws WrongTypeException If the given type is not found.
     * @throws WrongValueException If the values contained in the storage cannot be validated by the new type.
     */
    private function setType($type)
    {
        if (
            is_int($type)
            && !in_array($type, self::AVAILABLE_TYPES)
            || is_string($type)
            && !class_exists($type)
        ) {
            throw new WrongTypeException();
        }

        $this->setChecker(self::getCheckerFromType($type));
        $this->type = $type;

        return $this;
    }

    /**
     * Checker must be a callable accepting an entry (key + item) and returning true if it is accepted, false otherwise.
     *
     * @param callable $checker A callable with this signature:
     * <code>function (mixed $_item, int|string|null $_key, array $_this): bool</code>
     * where $_item is the item to add, $_key is the key used to add the item, and $_this the current state of the array.
     *
     * @return $this
     * @throws WrongValueException If the contained value cannot be validated by the new checker.
     */
    private function setChecker(callable $checker)
    {
        $this->checker = $checker;
        $this->type = null;

        foreach ($this->storage as $key => $item) {
            if (!$this->validateValue($item, $key)) {
                throw new WrongValueException();
            }
        }

        return $this;
    }

    /**
     * The value to be validated. Returns true if valid, false otherwise.
     *
     * @param mixed $item The value to be validated.
     * @param int|string|null $key The key used to set the entry.
     *
     * @return bool True if the value is valid, false otherwise.
     */
    private function validateValue($item, $key): bool
    {
        return is_callable($this->checker)
            ? ($this->checker)($item, $key, $this->toArray())
            : true
        ;
    }

    /**
     * Get the callable to check each value of the array.
     * @param int|string $type One of {@see TypedArray} constants or a fully-qualified class name.
     *
     * @return callable
     * @throws WrongTypeException
     */
    private static function getCheckerFromType($type): callable
    {
        $callable = null;
        if (is_int($type)) {
            switch ($type) {
                case self::TYPE_ARRAY:
                    $callable = 'is_array';
                    break;
                case self::TYPE_BOOL:
                    $callable = 'is_bool';
                    break;
                case self::TYPE_CALLABLE:
                    $callable = 'is_callable';
                    break;
                case self::TYPE_FLOAT:
                    $callable = 'is_float';
                    break;
                case self::TYPE_INT:
                    $callable = 'is_int';
                    break;
                case self::TYPE_ITERABLE:
                    $callable = 'is_iterable';
                    break;
                case self::TYPE_NULL:
                    $callable = 'is_null';
                    break;
                case self::TYPE_NUMERIC:
                    $callable = 'is_numeric';
                    break;
                case self::TYPE_OBJECT:
                    $callable = 'is_object';
                    break;
                case self::TYPE_RESOURCE:
                    $callable = 'is_resource';
                    break;
                case self::TYPE_SCALAR:
                    $callable = 'is_scalar';
                    break;
                case self::TYPE_STRING:
                    $callable = 'is_string';
                    break;
            }
        } else if (is_string($type)) {
            if (class_exists($type)) {
                $callable = function ($value) use ($type): bool {
                    return $value instanceof $type;
                };
            }
        }

        if (is_callable($callable)) {
            return function ($item, $key, $array) use ($callable): bool {
                return $callable($item);
            };
        }

        throw new WrongTypeException();
    }
}

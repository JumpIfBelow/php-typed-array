<?php

namespace TypedArray\Exception;

class WrongValueException extends TypedArrayException
{
    public $message = 'The value is invalidated by the checker.';
}

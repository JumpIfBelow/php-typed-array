<?php

namespace TypedArray\Exception;

class WrongTypeException extends TypedArrayException
{
    public $message = 'This type does not exists.';
}

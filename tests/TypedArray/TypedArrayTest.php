<?php

namespace TypedArray;

use PHPUnit\Framework\TestCase;
use TypedArray\Exception\WrongValueException;

class TypedArrayTest extends TestCase
{
    /**
     * @see TypedArray::__construct()
     */
    public function test__construct()
    {
        $t = new TypedArray(TypedArray::TYPE_INT);
        $this->assertEquals(TypedArray::TYPE_INT, $t->getType());

        $t = new TypedArray(\stdClass::class);
        $this->assertEquals(\stdClass::class, $t->getType());

        $t = new TypedArray(function ($_value): bool {
            return is_numeric($_value);
        });
        $t[] = 1;
        $t[] = 5.3;
        $this->expectException(WrongValueException::class);
        $t[] = true;
    }

    /**
     * @see TypedArray::getType()
     */
    public function testGetType()
    {
        $t = new TypedArray(TypedArray::TYPE_ARRAY);
        $this->assertEquals(TypedArray::TYPE_ARRAY, $t->getType());
    }

    /**
     * @see TypedArray::getChecker()
     */
    public function testGetChecker()
    {
        $t = new TypedArray(TypedArray::TYPE_ARRAY);
        $this->assertInstanceOf(\Closure::class, $t->getChecker());

        $t = new TypedArray(\stdClass::class);
        $this->assertInstanceOf(\Closure::class, $t->getChecker());
    }

    /**
     * @see TypedArray::setType()
     */
    public function testCheckMethod()
    {
        $t = new TypedArray(TypedArray::TYPE_BOOL);
        $t->setCheckMethod(TypedArray::TYPE_ARRAY);
        $this->assertEquals(TypedArray::TYPE_ARRAY, $t->getType());

        $this->assertSame($t, $t->setCheckMethod(TypedArray::TYPE_BOOL));
        $this->assertEquals(TypedArray::TYPE_BOOL, $t->getType());

        $t->setCheckMethod(TypedArray::class);
        $this->assertEquals(TypedArray::class, $t->getType());

        // dummy callback
        $callback = function($_item, $_key, array $_this): bool {
            return true;
        };
        $t->setCheckMethod($callback);
        $this->assertSame($t->getChecker(), $callback);
    }

    /**
     * @see TypedArray::clear()
     */
    public function testClear()
    {
        $t = new TypedArray(TypedArray::TYPE_INT);
        $t[] = 5;
        $t->clear();

        $this->assertEquals([], $t->toArray());
    }

    /**
     * @see TypedArray::toArray()
     */
    public function testToArray()
    {
        $t = new TypedArray(TypedArray::TYPE_INT);
        $t[] = 5;
        $t[] = 0;
        $t['associative'] = 3;

        $this->assertEquals(
            [
                5,
                0,
                'associative' => 3,
            ],
            $t->toArray()
        );
    }

    /**
     * @see TypedArray::offsetExists()
     */
    public function testOffsetExists()
    {
        $t = new TypedArray(TypedArray::TYPE_STRING);
        $t['set'] = 'exists';

        $this->assertArrayHasKey('set', $t);
        $this->assertArrayNotHasKey('not_set', $t);
    }

    /**
     * @see TypedArray::offsetGet()
     */
    public function testOffsetGet()
    {
        $t = new TypedArray(TypedArray::TYPE_INT);
        $value = 56;
        $t[0] = $value;

        $this->assertEquals($value, $t[0]);
    }

    /**
     * @see TypedArray::offsetSet()
     */
    public function testOffsetSet()
    {
        $t = new TypedArray(TypedArray::TYPE_INT);

        $t[] = 27;
        $t['offset'] = 40;
        $this->assertEquals(
            [
                27,
                'offset' => 40,
            ],
            $t->toArray()
        );
    }

    /**
     * @see TypedArray::offsetUnset()
     */
    public function testOffsetUnset()
    {
        $t = new TypedArray(TypedArray::TYPE_INT);

        $t[] = 0;
        $this->assertArrayHasKey(0, $t);

        unset($t[0]);
        $this->assertArrayNotHasKey(0, $t);
    }

    /**
     * @see TypedArray::count()
     */
    public function testCount()
    {
        $t = new TypedArray(TypedArray::TYPE_STRING);
        $this->assertEquals(0, count($t));

        $t[] = 'more';
        $this->assertEquals(1, count($t));

        $t[] = 'even more';
        $this->assertEquals(2, count($t));
    }

    /**
     * @see TypedArray::getIterator()
     */
    public function testGetIterator()
    {
        $t = new TypedArray(TypedArray::TYPE_INT);

        $t[] = 0;
        $t[] = 1;
        $t[] = 2;
        $t[56] = 56;

        foreach ($t as $key => $value) {
            $this->assertEquals($key, $value);
        }
    }
}
